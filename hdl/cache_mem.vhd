---------------------------------------------------------------------------------
-- filename: cache_mem.vhd
-- author  : Wladimir Schick
-- date    : 09/12/16
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.mips_pkg.all;
use work.CASTS.all;

entity cache_mem is
	generic(
		DATA_WIDTH   : integer  := 32;
		INDEX_WIDTH  : integer  := 8;
		TAG_WIDTH    : integer  := 4;  
		OFFSET_WIDTH : integer  := 4; -- in bits; determines # of bytes in a line
		EDGE         : EdgeType := RISING
	);
	port(
		-- in ports
		clk, we        : in  std_logic;
		addr           : in  std_logic_vector(TAG_WIDTH+INDEX_WIDTH+OFFSET_WIDTH-1 downto 0);
		din            : in  std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0);
		access_type    : in  AccessType;
		
		-- out ports
		is_hit,
		is_valid,
		is_dirty     : out std_logic := '0';
		tag_prev     : out std_logic_vector(TAG_WIDTH-1 downto 0)             := (others => '0');
		dout         : out std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0) := (others => '0')
	);
end entity cache_mem;

architecture arch of cache_mem is
	
	constant ADDR_WIDTH  : integer := TAG_WIDTH+INDEX_WIDTH+OFFSET_WIDTH;
	constant N_BYTES     : integer := 2**OFFSET_WIDTH;
	
	-- number of addition bytes to read/write
	constant HALF_OFFSET : integer := (DATA_WIDTH / 2 / 8) - 1; 
	constant WORD_OFFSET : integer := (DATA_WIDTH / 8) - 1;
	constant LINE_OFFSET : integer := N_BYTES - 1;
	
	type ByteArray is array(0 to N_BYTES-1) of std_logic_vector(7 downto 0);
	signal ibyte_in, ibyte_out : ByteArray := (others => (others => '0'));
		
	type IdxArray is array(0 to N_BYTES-1) of integer;
	
	function lbyte_indices (N : in integer) return IdxArray is
		variable idxs : IdxArray;
	begin
		for i in 0 to N-1 loop
			idxs(i) := i*8;
		end loop;	
		return idxs;
	end function lbyte_indices;
	
	function hbyte_indices (N : in integer) return IdxArray is
		variable idxs : IdxArray;
	begin
		for i in 0 to N-1 loop
			idxs(i) := (i+1)*8-1;
		end loop;	
		return idxs;
	end function hbyte_indices;
	
	signal hi_idx : IdxArray := hbyte_indices(N_BYTES);
	signal lo_idx : IdxArray := lbyte_indices(N_BYTES);
	
	signal tag_cpu,
	       tag_stored    : std_logic_vector(TAG_WIDTH-1   downto 0) := (others => '0');

	signal index         : std_logic_vector(INDEX_WIDTH-1 downto 0) := (others => '0');
	
	signal valid_vec, 
	       dirty_vec     : std_logic_vector((2**INDEX_WIDTH)-1 downto 0) := (others => '0');

	signal byte_we       : std_logic_vector(0 to N_BYTES-1):= (others => '0');
	
	signal index_i, byte_idx, access_offset, access_offset_reg, byte_idx_reg : integer := 0;
	
begin
	tag_area : entity work.dst_ram     -- is_hit is asynchronous
		       generic map(INDEX_WIDTH, TAG_WIDTH, EDGE, NO_CHANGE, "INIT_ZERO")
		       port map(clk, we, index, tag_cpu, tag_stored);

	data_area : for i in 0 to N_BYTES - 1 generate
		byte_i : entity work.bram
			     generic map(INDEX_WIDTH, 8, EDGE, NO_CHANGE, "INIT_ZERO")
			     port map(clk, byte_we(i), index, ibyte_in(i), ibyte_out(i));
	end generate;
	
	
	gen_signals : for i in 0 to N_BYTES-1 generate
		byte_we(i) <= we  when i >= byte_idx and i <= byte_idx+access_offset else 
		              '0';
		
		ibyte_in(i) <= din(hi_idx(i-byte_idx) downto lo_idx(i-byte_idx)) 
		when i >= byte_idx and i <= byte_idx+access_offset else x"00";
		              
		dout((i+1)*8-1 downto i*8) <= ibyte_out(i+byte_idx_reg) when i <= access_offset_reg else 
		                              x"00";
	end generate gen_signals;
	
	is_hit      <= '1' when tag_cpu = tag_stored and valid_vec(index_i) = '1' else '0';
	is_valid    <= valid_vec(index_i);
	is_dirty    <= dirty_vec(index_i);
	tag_prev    <= tag_stored;
	
	valid_vec(index_i) <= '1' when we = '1' and access_type = CacheLine and clk_edge(clk, EDGE);

	dirty_vec(index_i) <= '1' when we = '1' and access_type /= CacheLine and clk_edge(clk, EDGE) else 
	                      '0' when we = '1' and access_type  = CacheLine and clk_edge(clk, EDGE);
	
	tag_cpu      <= addr(ADDR_WIDTH-1 downto INDEX_WIDTH+OFFSET_WIDTH);
	index        <= addr(INDEX_WIDTH+OFFSET_WIDTH-1 downto OFFSET_WIDTH);
	index_i      <= to_i(index);
	
	byte_idx     <= 0 when access_type = CacheLine else to_i(addr(OFFSET_WIDTH-1 downto 0));
	
	access_offset  <= LINE_OFFSET when access_type = CacheLine else
	                  WORD_OFFSET when access_type = Word else
	                  HALF_OFFSET when access_type = Half else
	                  0;  -- Byte access
	                
	access_offset_reg <= access_offset when we = '0' and clk_edge(clk, EDGE);
	byte_idx_reg      <= byte_idx      when we = '0' and clk_edge(clk, EDGE);	
		
end architecture arch;


