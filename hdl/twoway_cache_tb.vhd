library IEEE;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
use work.CASTS.all;
use work.mips_pkg.all;

entity twoway_cache_tb is
end;

architecture test of twoway_cache_tb is
	constant DATA_WIDTH   : integer         := 32;
	constant INDEX_WIDTH  : integer         := 8;
	constant TAG_WIDTH    : integer         := 4;
	constant OFFSET_WIDTH : integer         := 4;
	constant ADDR_WIDTH   : integer         := TAG_WIDTH + INDEX_WIDTH + OFFSET_WIDTH;
	constant LINE_WIDTH   : integer         := (2**OFFSET_WIDTH)*8;
	constant EDGE         : EdgeType        := RISING;
	constant REPLACEMENT  : ReplacementType := LRU;

	signal clk, 
		   reset, 
	       hit,
	       rd_cpu, 
	       wr_cpu, 
	       ready_mem,  
	       wr_mem, 
	       rd_mem    : std_logic := '0';

	signal datato_mem, datafrom_mem : std_logic_vector(LINE_WIDTH-1 downto 0) := (others => '0');
	signal datato_cpu, datafrom_cpu : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
	signal addr_mem, addr_cpu       : std_logic_vector(ADDR_WIDTH-1 downto 0) := (others => '0');

	signal access_type : AccessType := CacheLine;

	signal hit_count, miss_count, test_case : integer := 0;

	type MemStateType is (INIT, IDLE, MEM_WRITE_BUSY, MEM_READ_BUSY, MEM_READY);
	signal memstate, memstatenext : MemStateType := INIT;

	type MemType is array (0 to 2**(TAG_WIDTH + INDEX_WIDTH) - 1) of std_logic_vector(LINE_WIDTH - 1 downto 0);
	signal mem : MemType := (others => (others => '0'));

begin

	memstate  <= memstatenext when rising_edge(clk);
	ready_mem <= '1' when memstate = MEM_READY else '0';

	-- generate clock
	process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

	-- simulate memory
	process(memstate, rd_mem, datato_mem, mem, addr_mem, wr_mem, clk)
		variable word0, word1, word2, word3 : std_logic_vector(DATA_WIDTH - 1 downto 0);
		variable line0, line1, line2, line3 : std_logic_vector(LINE_WIDTH - 1 downto 0);
		variable half0, half1               : std_logic_vector((DATA_WIDTH/2) - 1 downto 0);
		variable byte0, byte1               : std_logic_vector(7 downto 0);
		variable edges                      : integer := 0;

	begin
		-- simulate delay
		if (memstate = MEM_WRITE_BUSY or memstate = MEM_READ_BUSY) and rising_edge(clk) then
			edges := edges + 1;
		elsif memstate = MEM_READY or memstate = IDLE then
			edges := 0;
		end if;

		if memstate = INIT then

			byte0 := x"aa";
			byte1 := x"bb";

			half0 := x"cc_cc";
			half1 := x"dd_dd";

			word0 := half1 & half0;
			word1 := half0 & byte1 & byte0;
			word2 := x"11_11_11_11";
			word3 := x"22_22_22_22";
			-- mem address
			line0 := word3 & word2 & word1 & word0; -- 0_00_0
			line1 := word2 & word1 & word0 & word3; -- 0_01_0
			line2 := word1 & word0 & word3 & word2; -- 0_02_0
			line3 := word0 & word3 & word2 & word1; -- 1_02_0

			mem(to_i(x"0_00")) <= line0;
			mem(to_i(x"0_01")) <= line1;
			mem(to_i(x"0_02")) <= line2;
			mem(to_i(x"1_02")) <= line3;

			memstatenext <= IDLE;

		elsif memstate = IDLE and rd_mem = '1' then
			memstatenext <= MEM_READ_BUSY;

		elsif memstate = IDLE and wr_mem = '1' then
			memstatenext <= MEM_WRITE_BUSY;

		elsif memstate = MEM_READ_BUSY then
			if edges = 4 then
				datafrom_mem <= mem(to_i(addr_mem(ADDR_WIDTH - 1 downto OFFSET_WIDTH)));
				memstatenext <= MEM_READY;
			else
				memstatenext <= MEM_READ_BUSY;
			end if;

		elsif memstate = MEM_WRITE_BUSY then
			if edges = 4 then
				mem(to_i(addr_mem(ADDR_WIDTH - 1 downto OFFSET_WIDTH))) <= datato_mem;
				memstatenext                                            <= MEM_READY;
			else
				memstatenext <= MEM_WRITE_BUSY;
			end if;

		elsif memstate = MEM_READY then
			memstatenext <= IDLE;
		else
			memstatenext <= IDLE;
		end if;

	end process;

	sim : process is
		variable word0, word1, word2, word3 : std_logic_vector(DATA_WIDTH - 1 downto 0);
		variable line0, line1, line2, line3 : std_logic_vector(LINE_WIDTH - 1 downto 0);
		variable half0, half1               : std_logic_vector((DATA_WIDTH/2) - 1 downto 0);
		variable byte0, byte1               : std_logic_vector(7 downto 0);

	begin
		byte0 := x"aa";
		byte1 := x"bb";

		half0 := x"cc_cc";
		half1 := x"dd_dd";

		word0 := half1 & half0;         -- ddddcccc
		word1 := half0 & byte1 & byte0; -- ccccbbaa
		word2 := x"11_11_11_11";
		word3 := x"22_22_22_22";
		-- mem address
		line0 := word3 & word2 & word1 & word0; -- addr(0_00_0)
		line1 := word2 & word1 & word0 & word3; -- addr(0_01_0)
		line2 := word1 & word0 & word3 & word2; -- addr(0_02_0)
		line3 := word0 & word3 & word2 & word1; -- addr(1_02_0)

		wait for 2 ns;

		if REPLACEMENT = RANDOM then

			-- Test 1:
			-- read word0 from line0 (read word miss)
			-- expect rnd_bit = '0' and block_select(0) = '0'
			-- flip rnd_bit to '1' after clock edge
			-- expect is_hit = "10"
			-- expected output : ddddcccc
			wait until rising_edge(clk);
			test_case   <= 1;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"0_00_0";

			wait for 2 ns;
			assert hit = '0' report "stall_cpu unexpected result" severity FAILURE;

			wait until hit = '1';
			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word0 report "datato_cpu unexpected value" severity FAILURE;

			-- Test 2
			-- read word3 from line1 (read word miss)
			-- expect rnd_bit = '1' and block_select(0) = '1'
			-- flip rnd_bit to '0' after clock edge
			-- expect is_hit = "10"
			-- expected output : 22222222
			wait until falling_edge(clk);
			test_case   <= 2;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"0_01_0";

			wait for 2 ns;
			assert hit = '0' report "stall_cpu unexpected result" severity FAILURE;

			wait until hit = '1';
			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word3 report "datato_cpu unexpected value" severity FAILURE;

			-- Test 3
			-- write word0 in place of word2 in line2 (write word miss)
			-- expect rnd_bit = '0' and block_select(0) = '0'
			-- expect is_hit = "10"
			-- flip rnd_bit to '1' after clock edge
			wait until falling_edge(clk);
			test_case    <= 3;
			rd_cpu       <= '0';
			wr_cpu       <= '1';
			access_type  <= Word;
			datafrom_cpu <= word0;
			addr_cpu     <= x"0_02_0";

			wait for 2 ns;
			assert hit = '0' report "stall_cpu unexpected result" severity FAILURE;

			wait until hit = '1';
			wait until rising_edge(clk);

			-- Test 4
			-- read the newly written word0 (read word hit)
			-- expect rnd_bit = '1' and block_select(0) = '1'
			-- no flip of rnd_bit after clock edge
			-- expect is_hit) = "10" and is_hit(1) = '0'
			-- expected output: ddddcccc
			wait until falling_edge(clk);
			test_case   <= 4;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"0_02_0";

			wait for 2 ns;
			assert hit = '1' report "stall_cpu unexpected result" severity FAILURE;

			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word0 report "datato_cpu unexpected value" severity FAILURE;

		else  -- LRU
		
			-- Test 1:
			-- read word2 from line2 (read word miss)
			-- expect usebit_vec(2) = '0' and block_select(0) = '1'
			-- after the miss is resolved and a hit occurs expect usebit_vec(2) = '1' after clock edge
			-- expect is_hit = "10"
			-- expected output : 11111111
			wait until rising_edge(clk);
			test_case   <= 1;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"0_02_0";

			wait for 2 ns;
			assert hit = '0' report "stall_cpu unexpected result" severity FAILURE;

			wait until hit = '1';
			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word2 report "datato_cpu unexpected value" severity FAILURE;

			-- Test 2
			-- read word1 from line3 (read word miss)
			-- expect usebit_vec(2) = '1' and block_select(2) = '0'
			-- after the miss is resolved and a hit occurs expect usebit_vec(2) = '0' after clock edge
			-- expect is_hit = "01"
			-- expected output : ccccbbaa
			wait until falling_edge(clk);
			test_case   <= 2;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"1_02_0";

			wait for 2 ns;
			assert hit = '0' report "stall_cpu unexpected result" severity FAILURE;

			wait until hit = '1';
			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word1 report "datato_cpu unexpected value" severity FAILURE;

			-- Test 3
			-- write word0 in place of word1 in line3 (write word hit)
			-- expect usebit_vec(2) = '0' (since line3 is referenced) after clock edge
			-- expect is_hit = "01"
			wait until falling_edge(clk);
			test_case    <= 3;
			rd_cpu       <= '0';
			wr_cpu       <= '1';
			access_type  <= Word;
			datafrom_cpu <= word0;
			addr_cpu     <= x"1_02_0";

			wait for 2 ns;
			assert hit = '1' report "stall_cpu unexpected result" severity FAILURE;
			wait until rising_edge(clk);

			-- Test 4
			-- write word3 in place of word2 in line2 (write word hit)
			-- expect usebit_vec(2) = '1' (since line2 is referenced) after clock edge
			-- expect is_hit = "10"
			wait until falling_edge(clk);
			test_case    <= 4;
			rd_cpu       <= '0';
			wr_cpu       <= '1';
			access_type  <= Word;
			datafrom_cpu <= word3;
			addr_cpu     <= x"0_02_0";

			wait for 2 ns;
			assert hit = '1' report "stall_cpu unexpected result" severity FAILURE;
			wait until rising_edge(clk);
			
			
			-- Test 5
			-- read the newly written word3 from line2 (read word hit)
			-- expect usebit_vec(2) = '1' (since line2 is referenced) after clock edge
			-- expect is_hit = "10"
			-- expected output: ddddcccc
			wait until falling_edge(clk);
			test_case   <= 5;
			rd_cpu      <= '1';
			wr_cpu      <= '0';
			access_type <= Word;
			addr_cpu    <= x"0_02_0";

			wait for 2 ns;
			assert hit = '1' report "stall_cpu unexpected result" severity FAILURE;

			wait until rising_edge(clk);
			wait for 2 ns;
			assert datato_cpu = word3 report "datato_cpu unexpected value" severity FAILURE;

		end if;

		wait;

	end process sim;

	dut : entity work.twoway_cache
		generic map(
			DATA_WIDTH   => DATA_WIDTH,
			ADDR_WIDTh   => ADDR_WIDTH,
			INDEX_WIDTH  => INDEX_WIDTH,
			OFFSET_WIDTH => OFFSET_WIDTH,
			EDGE         => EDGE,
			REPLACEMENT  => REPLACEMENT
		)
		port map(
			clk          => clk,
			reset        => reset,
			rd_cpu       => rd_cpu,
			wr_cpu       => wr_cpu,
			ready_mem    => ready_mem,
			access_type  => access_type,
			addr_cpu     => addr_cpu,
			datafrom_cpu => datafrom_cpu,
			datafrom_mem => datafrom_mem,
			hit          => hit,
			rd_mem       => rd_mem,
			wr_mem       => wr_mem,
			addr_mem     => addr_mem,
			datato_cpu   => datato_cpu,
			datato_mem   => datato_mem,
			hit_count    => hit_count,
			miss_count   => miss_count
		);

end;
