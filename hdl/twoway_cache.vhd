library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
use work.mips_pkg.all;
use work.CASTS.all;



entity twoway_cache is
	generic(
		DATA_WIDTH    : integer         := 32;  -- instruction/data word width
		ADDR_WIDTH    : integer         := 12;
		INDEX_WIDTH   : integer         := 8;	 -- in bits; defines the depth of the cache
		OFFSET_WIDTH  : integer         := 2;   -- in bits; defines the number of bytes in a line
		EDGE          : EdgeType        := RISING;
		REPLACEMENT   : ReplacementType := LRU
	);

	port(
		-- in ports
		clk, 
		reset,
		rd_cpu, 
		wr_cpu, 
		ready_mem    : in  std_logic;
		
		access_type  : in  AccessType;
		addr_cpu     : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
		datafrom_cpu : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		datafrom_mem : in  std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0);
		
		-- out ports
		hit,
		rd_mem, 
		wr_mem       : out std_logic := '0';
		
		addr_mem     : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
		datato_cpu   : out std_logic_vector(DATA_WIDTH - 1 downto 0)          := (others => '0');
		datato_mem   : out std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0) := (others => '0');
		
		hit_count,
		miss_count   : out integer := 0
	);

end entity;

architecture arch of twoway_cache is

	constant LINE_WIDTH : integer := (2**OFFSET_WIDTH)*8; 
	constant TAG_WIDTH  : integer := ADDR_WIDTH - INDEX_WIDTH - OFFSET_WIDTH;
	
	constant ZERO_OFFSET       : std_logic_vector(OFFSET_WIDTH-1 downto 0)        := (others => '0');
	constant ZERO_DATA_PADDING : std_logic_vector(LINE_WIDTH-1 downto DATA_WIDTH) := (others => '0');
	
	-- states of the FSM
	type StateType is (
		CPU_REQUEST, ALLOCATE, WRITE_BACK, READ_LINE_CACHE
	);
	signal state, nextstate : StateType := CPU_REQUEST;

	signal block_select : std_logic_vector(0 to 0)  := (others => '0');
	
	signal we_cache, dirty_cache, 
	       valid_cache, hit_cache, we_vec : std_logic_vector(1 downto 0) := (others => '0');
	
	signal din_cache, dout_cache0, 
	       dout_cache1, dout_cache : std_logic_vector(LINE_WIDTH-1 downto 0) := (others => '0');
	
	signal addr_cache : std_logic_vector(ADDR_WIDTH-1 downto 0) := (others => '0');
	
	signal tag_cache0, tag_cache1, 
	       tag_cpu, tag_cache, tag_reg : std_logic_vector(TAG_WIDTH-1 downto 0)   := (others => '0');
	
	signal index_cpu, index_reg : std_logic_vector(INDEX_WIDTH-1 downto 0)  := (others => '0');
	signal usebit_vec           : std_logic_vector(0 to (2**INDEX_WIDTH)-1) := (others => '0');	
	
	signal last_alloc : std_logic_vector(TAG_WIDTH+INDEX_WIDTH-1 downto 0) := (others => '0'); 
	
	signal bhwl_cache          : AccessType := Word; -- byte/half/word/line
	signal valid_cpu_request   : boolean    := FALSE;

	signal hit_cntr, miss_cntr, selector : integer    := 0;
	
	signal 	valid_hit, 
			valid_miss, 
	       	rnd_bit, 
	       	last_miss, 
	       	valid_output : std_logic := '0';

begin
	
	index_cpu  <= addr_cpu(INDEX_WIDTH+OFFSET_WIDTH-1 downto OFFSET_WIDTH);
	index_reg  <= index_cpu when clk_edge(clk, EDGE) and valid_miss = '1';
	
	tag_cpu    <= addr_cpu(ADDR_WIDTH-1 downto INDEX_WIDTH+OFFSET_WIDTH);
	tag_reg    <= tag_cpu when clk_edge(clk, EDGE) and valid_miss = '1';
	
	
	hit        <= valid_hit;
	hit_count  <= hit_cntr;
	miss_count <= miss_cntr;
	
	valid_output <= '1' when clk_edge(clk, EDGE) and valid_hit = '1' else
	                '0' when clk_edge(clk, EDGE) and valid_hit = '0';	
	
	datato_cpu <= dout_cache(DATA_WIDTH-1 downto 0) when valid_output = '1' else
	              (others => '0')                   when valid_output = '0';
	
	selector   <= 0 when clk_edge(clk, EDGE) and hit_cache(0) = '1' else
	              1 when clk_edge(clk, EDGE) and hit_cache(1) = '1';
	
	dout_cache <= dout_cache0 when selector = 0 else
	              dout_cache1 when selector = 1;
	
	valid_hit  <= '1' when state = CPU_REQUEST and valid_cpu_request and hit_cache /= "00" else
	              '0';
	
	valid_miss <= '1' when state = CPU_REQUEST and valid_cpu_request and hit_cache = "00" else
	              '0';   
	
	miss_cntr  <= miss_cntr+1 when valid_miss = '1' and clk_edge(clk, EDGE) else
	              0           when reset = '1';
	
	last_alloc <= (tag_reg & index_reg) when state = ALLOCATE and nextstate = CPU_REQUEST;
	last_miss  <= '1' when clk_edge(clk, EDGE) and valid_miss = '1' else
	              '0' when clk_edge(clk, EDGE) and valid_hit  = '1';
	
	hit_cntr   <=  hit_cntr+1 when clk_edge(clk, EDGE) and valid_hit = '1' and 
	                               not (last_miss = '1' and (tag_cpu & index_cpu) =  last_alloc) else
	               0          when reset = '1';
		
	tag_cache   <= tag_cache0 when block_select(0) = '0' else
	               tag_cache1 when block_select(0) = '1';
	
	valid_cpu_request <= TRUE when (rd_cpu = '1' and wr_cpu = '0') or
	                               (rd_cpu = '0' and wr_cpu = '1') else
	                     FALSE;
	
	state <= CPU_REQUEST when reset = '1' else 
	         nextstate   when clk_edge(clk, EDGE);

	repl_strategy : process(clk, hit_cache(0), hit_cache(1), index_cpu, reset, rnd_bit, state, usebit_vec, valid_hit, valid_miss)
	begin
		if REPLACEMENT = RANDOM then			
			if valid_miss = '1' and clk_edge(clk, EDGE) then
				rnd_bit <= not rnd_bit; -- flip the bit
			elsif reset = '1' then
				rnd_bit <= '0';
			end if;
			
			if state = CPU_REQUEST and not clk_edge(clk, EDGE) then 
				block_select(0) <= rnd_bit;
			end if;
			
		elsif REPLACEMENT = LRU then
			if valid_hit = '1' and clk_edge(clk, EDGE) then
				if hit_cache(0) = '1' then
					usebit_vec(TO_I(index_cpu)) <= '0';
				elsif hit_cache(1) = '1' then
					usebit_vec(TO_I(index_cpu)) <= '1';
				end if;
			end if;
			
			-- usebit_vec is updated after clk edge therefore select
			-- the block to replace before the edge
			if state = CPU_REQUEST and not clk_edge(clk, EDGE) then 
				if usebit_vec(TO_I(index_cpu)) = '0' then
					block_select(0) <= '1'; -- if block0 was recently used select block1
				elsif usebit_vec(TO_I(index_cpu)) = '1' then
					block_select(0) <= '0'; -- if block1 was recently used select block0
				end if;
			end if;
		end if;
	end process repl_strategy;	
	
	
	transition_logic : process(state, valid_cpu_request, hit_cache, dirty_cache, valid_cache, ready_mem, block_select)
	begin
		case state is
			when CPU_REQUEST =>
				if valid_cpu_request then
					if hit_cache /= "00" then
						nextstate <= CPU_REQUEST;
					else  -- miss
						if dirty_cache(TO_I(block_select)) = '1' and valid_cache(TO_I(block_select)) = '1' then
							nextstate <= READ_LINE_CACHE;  -- read line before writing back
						else
							nextstate <= ALLOCATE;
						end if;
					end if;
				else
					nextstate <= CPU_REQUEST;
				end if;

			when ALLOCATE =>
				if ready_mem = '1' then
					nextstate <= CPU_REQUEST;
				else
					nextstate <= ALLOCATE;
				end if;

			when READ_LINE_CACHE =>
				nextstate <= WRITE_BACK;

			when WRITE_BACK =>
				if ready_mem = '1' then
					nextstate <= ALLOCATE;
				else
					nextstate <= WRITE_BACK;
				end if;
		end case;
	end process;

	
	-- outputs to main memory
	datato_mem <= dout_cache0 when block_select(0) = '0' else
	              dout_cache1 when block_select(0) = '1';
	             
	rd_mem    <= '1' when (state = CPU_REQUEST and nextstate = ALLOCATE) or
	                      (state = WRITE_BACK  and nextstate = ALLOCATE) or
	                      (state = ALLOCATE    and nextstate = ALLOCATE) else
	             '0';
	
	wr_mem    <= '1' when (state = READ_LINE_CACHE and nextstate = WRITE_BACK) or
	                      (state = WRITE_BACK      and nextstate = WRITE_BACK) else
	             '0';                     

	addr_mem   <=  tag_cache & index_reg & ZERO_OFFSET 
				   when 
	               		(state = READ_LINE_CACHE and nextstate = WRITE_BACK) or
						(state = WRITE_BACK      and nextstate = WRITE_BACK) 
				   else
				   
				   tag_cpu & index_cpu & ZERO_OFFSET
			       when 
			       	    (state = CPU_REQUEST and nextstate = ALLOCATE) 
			       else
			       tag_reg & index_reg & ZERO_OFFSET;   
	             
	
	-- outputs to cache memory
	addr_cache <= tag_reg & index_reg & ZERO_OFFSET when state     = ALLOCATE    and 
	                                                     nextstate = CPU_REQUEST else
	              addr_cpu;
	
	we_vec    <= "01" when block_select(0) = '0' else
	             "10" when block_select(0) = '1';	
	
	we_cache  <= hit_cache(1) & hit_cache(0) when state = CPU_REQUEST and nextstate = CPU_REQUEST and 
	                                              valid_cpu_request   and wr_cpu    = '1'         else 
	             we_vec                      when state = ALLOCATE    and nextstate = CPU_REQUEST else
	             "00";
	        
	din_cache  <= datafrom_mem when state = ALLOCATE and nextstate = CPU_REQUEST else
	              ZERO_DATA_PADDING & datafrom_cpu;

	bhwl_cache <= access_type when state = CPU_REQUEST and nextstate = CPU_REQUEST else
	              CacheLine;

	block0 : entity work.cache_mem
		generic map(
			DATA_WIDTH   => DATA_WIDTH,
			INDEX_WIDTH  => INDEX_WIDTH,
			TAG_WIDTH    => TAG_WIDTH,
			OFFSET_WIDTH => OFFSET_WIDTH,
			EDGE         => EDGE
		)
		port map(
			clk         => clk,
			we          => we_cache(0),
			addr        => addr_cache,
			din         => din_cache,
			access_type => bhwl_cache,
			is_hit      => hit_cache(0),
			is_valid    => valid_cache(0),
			is_dirty    => dirty_cache(0),
			tag_prev    => tag_cache0,
			dout        => dout_cache0
		);
		
		
	block1 : entity work.cache_mem
		generic map(
			DATA_WIDTH   => DATA_WIDTH,
			INDEX_WIDTH  => INDEX_WIDTH,
			TAG_WIDTH    => TAG_WIDTH,
			OFFSET_WIDTH => OFFSET_WIDTH,
			EDGE         => EDGE
		)
		port map(
			clk         => clk,
			we          => we_cache(1),
			addr        => addr_cache,
			din         => din_cache,
			access_type => bhwl_cache,
			is_hit      => hit_cache(1),
			is_valid    => valid_cache(1),
			is_dirty    => dirty_cache(1),
			tag_prev    => tag_cache1,
			dout        => dout_cache1
		);

end architecture arch;

