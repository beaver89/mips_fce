library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
use work.mips_pkg.all;
use work.CASTS.all;

entity dm_cache is
	generic(
		DATA_WIDTH    : integer  := 32;  -- instruction/data word width
		ADDR_WIDTH    : integer  := 32;
		INDEX_WIDTH   : integer  := 8;	 -- in bits; defines the depth of the cache
		OFFSET_WIDTH  : integer  := 2;   -- in bits; defines the number of bytes in a line
		EDGE          : EdgeType := RISING
	);

	port(
		-- in ports
		clk, 
		reset,
		rd_cpu, 
		wr_cpu, 
		ready_mem    : in  std_logic;
		
		access_type  : in  AccessType;
		addr_cpu     : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
		datafrom_cpu : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		datafrom_mem : in  std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0);
		
		-- out ports
		hit,
		rd_mem, 
		wr_mem       : out std_logic := '0';
		
		addr_mem     : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
		datato_cpu   : out std_logic_vector(DATA_WIDTH - 1 downto 0)          := (others => '0');
		datato_mem   : out std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0) := (others => '0');
		
		hit_count,
		miss_count   : out integer := 0
	);

end entity;

architecture arch of dm_cache is

	constant LINE_WIDTH : integer := (2**OFFSET_WIDTH)*8; 
	constant TAG_WIDTH  : integer := ADDR_WIDTH - INDEX_WIDTH - OFFSET_WIDTH;
	
	constant ZERO_OFFSET       : std_logic_vector(OFFSET_WIDTH-1 downto 0)        := (others => '0');
	constant ZERO_DATA_PADDING : std_logic_vector(LINE_WIDTH-1 downto DATA_WIDTH) := (others => '0');
	
	-- states of the FSM
	type StateType is (
		CPU_REQUEST, ALLOCATE, WRITE_BACK, READ_LINE_CACHE
	);
	signal state, nextstate : StateType := CPU_REQUEST;

	signal bhwl_cache : AccessType := Word; -- byte/half/word/line
	       
	signal din_cache, dout_cache   : std_logic_vector(LINE_WIDTH-1 downto 0)  := (others => '0');
	signal addr_cache              : std_logic_vector(ADDR_WIDTH-1 downto 0)  := (others => '0');
	signal tag_cache, tag, tag_reg : std_logic_vector(TAG_WIDTH-1 downto 0)   := (others => '0');
	signal index, index_reg        : std_logic_vector(INDEX_WIDTH-1 downto 0) := (others => '0');
	signal last_alloc              : std_logic_vector(TAG_WIDTH+INDEX_WIDTH-1 downto 0) := (others => '0');
	
	signal valid_cpu_request : boolean := FALSE;
	
	signal hit_cntr, miss_cntr : integer := 0;
	
	signal	we_cache, 
			dirty_cache, 
			valid_cache, 
			hit_cache, 
			valid_hit, 
			valid_miss, 
			last_miss, 
			valid_output : std_logic := '0';


begin
	
	index      <= addr_cpu(INDEX_WIDTH+OFFSET_WIDTH-1 downto OFFSET_WIDTH);
	index_reg  <= index when clk_edge(clk, EDGE) and valid_miss = '1';
	
	tag        <= addr_cpu(ADDR_WIDTH-1 downto INDEX_WIDTH+OFFSET_WIDTH);
	tag_reg    <= tag when clk_edge(clk, EDGE) and valid_miss = '1';
	
	hit        <= valid_hit;
	hit_count  <= hit_cntr;
	miss_count <= miss_cntr;
	
	valid_output <= '1' when clk_edge(clk, EDGE) and valid_hit = '1' else
	                '0' when clk_edge(clk, EDGE) and valid_hit = '0';
	
	datato_cpu <= dout_cache(DATA_WIDTH-1 downto 0) when valid_output = '1' else
	              (others => '0')                   when valid_output = '0';
	
	valid_hit  <= '1' when state = CPU_REQUEST and valid_cpu_request and hit_cache = '1' else
	              '0';      
	
	valid_miss <= '1' when state = CPU_REQUEST and valid_cpu_request and hit_cache = '0' else
	              '0';              
	
	miss_cntr  <= miss_cntr+1 when valid_miss = '1' and clk_edge(clk, EDGE) else
	              0           when reset = '1';
	
	last_alloc <= (tag_reg & index_reg) when state = ALLOCATE and nextstate = CPU_REQUEST;
	last_miss  <= '1' when clk_edge(clk, EDGE) and valid_miss = '1' else
	              '0' when clk_edge(clk, EDGE) and valid_hit  = '1';
	
	hit_cntr   <=  hit_cntr+1 when clk_edge(clk, EDGE)  and valid_hit     = '1'          and 
	                               not (last_miss = '1' and (tag & index) =  last_alloc) else
	               0          when reset = '1';
	
	valid_cpu_request <= TRUE when (rd_cpu = '1' and wr_cpu = '0') or
	                               (rd_cpu = '0' and wr_cpu = '1') else
	                     FALSE;
	
	state <= CPU_REQUEST when reset = '1' else 
	         nextstate   when clk_edge(clk, EDGE);

	transition_logic : process(state, dirty_cache, valid_cache, ready_mem, valid_hit, valid_miss)
	begin
		case state is
			when CPU_REQUEST =>
				if valid_hit = '1' then
					nextstate <= CPU_REQUEST;
				elsif valid_miss = '1' then
					if dirty_cache = '1' and valid_cache = '1' then
						nextstate <= READ_LINE_CACHE; -- read line before writing back
					else
						nextstate <= ALLOCATE;
					end if;
				else
					nextstate <= CPU_REQUEST; -- not valid cpu request
				end if;

			when ALLOCATE =>
				if ready_mem = '1' then
					nextstate <= CPU_REQUEST;
				else
					nextstate <= ALLOCATE;
				end if;

			when READ_LINE_CACHE =>
				nextstate <= WRITE_BACK;
			
			when WRITE_BACK =>
				if ready_mem = '1' then
					nextstate <= ALLOCATE;
				else
					nextstate <= WRITE_BACK;
				end if;
			
		end case;
	end process;

	
	-- outputs to main memory
	datato_mem <= dout_cache;
	
	rd_mem     <= '1' when (state = CPU_REQUEST and nextstate = ALLOCATE) or
	                       (state = WRITE_BACK  and nextstate = ALLOCATE) or
	                       (state = ALLOCATE    and nextstate = ALLOCATE) else
	              '0';
	
	wr_mem     <= '1' when (state = READ_LINE_CACHE and nextstate = WRITE_BACK) or
	                       (state = WRITE_BACK      and nextstate = WRITE_BACK) else
	              '0';                     

	addr_mem   <=  tag_cache & index_reg & ZERO_OFFSET 
				   when 
	               		(state = READ_LINE_CACHE and nextstate = WRITE_BACK) or
						(state = WRITE_BACK      and nextstate = WRITE_BACK) 
				   else
				   
				   tag & index & ZERO_OFFSET
			       when 
			       	    (state = CPU_REQUEST and nextstate = ALLOCATE) 
			       else
			       tag_reg & index_reg & ZERO_OFFSET;        
	             
	-- outputs to cache memory
	addr_cache <= tag_reg & index_reg & ZERO_OFFSET when state = ALLOCATE and nextstate = CPU_REQUEST else
	              addr_cpu;
	
	we_cache   <= '1' when (state = CPU_REQUEST and valid_hit = '1' and wr_cpu = '1') or 
	                       (state = ALLOCATE    and nextstate = CPU_REQUEST)          else
	              '0';
	        
	din_cache  <= datafrom_mem when state = ALLOCATE and nextstate = CPU_REQUEST else
	              ZERO_DATA_PADDING & datafrom_cpu;

	bhwl_cache <= access_type when state = CPU_REQUEST and valid_hit = '1' else
	              CacheLine;
	
	cache_memory : entity work.cache_mem
		generic map(
			DATA_WIDTH   => DATA_WIDTH,
			INDEX_WIDTH  => INDEX_WIDTH,
			TAG_WIDTH    => TAG_WIDTH,
			OFFSET_WIDTH => OFFSET_WIDTH,
			EDGE         => EDGE
		)
		port map(
			clk         => clk,
			we          => we_cache,
			addr        => addr_cache,
			din         => din_cache,
			access_type => bhwl_cache,
			is_hit      => hit_cache,
			is_valid    => valid_cache,
			is_dirty    => dirty_cache,
			tag_prev    => tag_cache,
			dout        => dout_cache
		);

end architecture arch;
