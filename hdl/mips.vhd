---------------------------------------------------------------------------------
-- filename: mips.vhd
-- author  : Wolfgang Brandt
-- company : TUHH, Institute of embedded systems
-- revision: 0.1
-- date    : 26/11/15   
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.mips_pkg.all;
use work.casts.all;

entity mips is             -- Pipelined MIPS processor   
	generic(
		DFileName : STRING := "../dmem/isort_pipe";
		IFileName : STRING := "../imem/isort_pipe");
	port(
		clk, reset         : in  STD_LOGIC;
		writedata, dataadr : out STD_LOGIC_VECTOR(31 downto 0);
		memwrite           : out STD_LOGIC
	);
end;

architecture struct of mips is
	signal zero, 
	       lez, 
	       ltz, 
	       gtz, 
	       branch,
	       stall,
	       flush,
	       wr_btb,
	       hit_btb     : STD_LOGIC := '0';
	 
	signal pc,
	       pcjump,
	       pcbranch, 
	       nextpc,
	       pc4,
	       a,
	       signext,
	       b,
	       rd2imm,
	       aluout,
	       wd,
	       rd,
	       rd1,
	       rd2,
	       aout,
	       WB_wd,
	       WB_rd,
	       IF_ir,
	       sum,
	       newentry_btb,
	       entry_btb : STD_LOGIC_VECTOR(31 downto 0) := ZERO32;
	
	signal c         : ControlType     := C_INIT;
	signal i         : InstructionType := I_INIT;
	signal ID        : IDType          := ID_INIT;
	signal EX        : EXType          := EX_INIT;
	signal MA        : MAType          := MA_INIT;
	signal WB        : WBType          := WB_INIT;
	
	signal wa, EX_Rd : STD_LOGIC_VECTOR(4 downto 0) := "00000";
	signal MA_Rd     : STD_LOGIC_VECTOR(4 downto 0) := "00000";
	
	signal forwardA, 
	       forwardB  : ForwardType := FromREG;
	
	-- TODO change this
	signal invb, twosC_b : SIGNED(31 downto 0);
	constant zeros31     : STD_LOGIC_VECTOR (30 downto 0) := (others => '0');
  	
  	signal BR, BR_REG : BranchType := BR_INIT; 
	
	
	type IntArrayType is array (integer range <>) of integer;
	
	signal correct : IntArrayType(0 to 31) := (
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	);
	signal incorrect : IntArrayType(0 to 31) := (
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	);
	
	signal update_bht, 
	       take_bht,
	       wastaken_bht,
	       takeandhit    : std_logic := '0';
	
	signal index_bht,
	       indexwd_bht : std_logic_vector(4 downto 0) := (others => '0');
	       
	signal pc_btb, 
	       pc_restarted, 
	       new_pc, pcinc : std_logic_vector(31 downto 0) := (others => '0');
	
	signal pc_select : integer := 0;
	
	signal first_clock : std_logic := '1';
	
	signal jpi, 
	       isit_jpi_btb, 
	       is_brinst     : std_logic := '0';
	
	
	
	signal rd_cache,  
		   ready_mem,
		   hit_cache,
		   rd_mem, 
		   wr_mem       :std_logic;

	signal addr_cache     : std_logic_vector(11 downto 0) := (others => '0');
	signal datafrom_mem : std_logic_vector(31 downto 0) := (others => '0');	
	signal addr_mem     : std_logic_vector(11 downto 0) := (others => '0');
	signal dout_cache   : std_logic_vector(31 downto 0) := (others => '0');
		
	signal hit_count,
		   miss_count,
		   counter      : integer := 0;
	signal pc4reg : STD_LOGIC_VECTOR(31 downto 0);
	signal ir_select : integer := 0;
	signal entry_btb_reg : STD_LOGIC_VECTOR(31 downto 0);

	
begin
	first_clock <= '0' when rising_edge(clk) and first_clock = '1';
	
	-------------------- Instruction Fetch Phase (IF) -----------------------------
	
	pc     <= (others => '0') when pc_select = 0 else
	          pc_restarted    when pc_select = 1 else
			  pc              when pc_select = 2 else
	          entry_btb       when pc_select = 3 else
	          pcinc           when pc_select = 4 else     
	          (others => 'X') when pc_select = 5; -- for debugging

	pc_select <= 0 when rising_edge(clk) and first_clock = '1' else -- to ensure that the pc starts with 0
	             1 when rising_edge(clk) and flush = '1'       else
	             2 when rising_edge(clk) and (stall = '1' or hit_cache = '0') else -- stall pc; pc = pc
	             3 when rising_edge(clk) and takeandhit = '1' else -- pc = btb_entry
	             4 when rising_edge(clk) and takeandhit = '0' else -- pc = pc4
	             5 when rising_edge(clk); 
	
	
	pcinc        <= pc4 when rising_edge(clk);
	pc4          <= to_slv(unsigned(pc) + 4);
	pc_restarted <= new_pc when rising_edge(clk); -- new pc when instruction is flushed
	
	
	takeandhit <= '1' when (hit_btb = '1' and take_bht = '1')     or
	                       (hit_btb = '1' and isit_jpi_btb = '1') else -- jump instructions are always assumed as taken
	              '0';
	
	index_bht <= pc(6 downto 2);
	
	BHT_inst : entity work.BHT
		port map(
			clk            => clk,
			update         => update_bht,
			was_taken      => wastaken_bht,
			index          => index_bht,
			indexto_update => indexwd_bht,
			take           => take_bht
		);
			
	
	pc_btb <= pc when wr_btb = '0' else
			  BR.pc; 
	jpi    <= '1' when c.jr = '1' or c.jump = '1' else
	          '0'; 
	
	BTB_inst : entity work.BTB
		port map(
			clk         => clk,
			wr_btb      => wr_btb,
			jpi         => jpi,
			pc          => pc_btb,
			btac_entry  => newentry_btb, -- to be written
			target_addr => entry_btb,    -- stored target address
			hit_btac    => hit_btb,
			isit_jpi    => isit_jpi_btb	
		);
	
	
	imem : entity work.bram
		-- Falling edge to fetch the instruction during current clock cycle
		generic map(INIT => (IFileName & ".imem"), EDGE => Falling) 
		port map(clk, '0', addr_mem(11 downto 2), (others => '0'), datafrom_mem);

	
	addr_cache <= (pc(11 downto 2) & "00");
	rd_cache <= '0' when flush = '1' else -- if flush and it is a miss do not resolve it
	            '1';
	counter <= counter+1 when falling_edge(clk) and rd_mem = '1' else
	           0         when rd_mem = '0' and clk'event;
	    
	ready_mem <= '1' when counter = 5 else '0'; -- just to simulate delay of main memory
	
	
	dm_cache_inst : entity work.dm_cache
		generic map(
			DATA_WIDTH   => 32,
			ADDR_WIDTH   => 12,
			INDEX_WIDTH  => 8,
			OFFSET_WIDTH => 2,
			EDGE         => Rising
		)
		port map(
			clk          => clk,
			reset        => '0',
			rd_cpu       => rd_cache,
			wr_cpu       => '0',
			ready_mem    => ready_mem,
			access_type  => Word,
			addr_cpu     => addr_cache,
			datafrom_cpu => (others => 'Z'),
			datafrom_mem => datafrom_mem,
			hit          => hit_cache,
			rd_mem       => rd_mem,
			addr_mem     => addr_mem,
			datato_cpu   => dout_cache,
			hit_count    => hit_count,
			miss_count   => miss_count
		);														    
	
	
	
	pc4reg <= pc4    when rising_edge(clk) and hit_cache = '1' and stall = '0' and flush = '0' else
	          pc4reg when rising_edge(clk) and stall = '1'                                     else
	          ZERO32 when rising_edge(clk);
	
	BR_REG <= (take_bht, hit_btb, takeandhit, pc) when rising_edge(clk) and hit_cache = '1' and stall = '0' and flush = '0' else
	          BR_REG when rising_edge(clk) and stall = '1'                                     else
	          BR_INIT when rising_edge(clk);
	
	
	
	ir_select <= 0 when rising_edge(clk) and hit_cache = '1' and stall = '0' and flush = '0' else
	             1 when rising_edge(clk) and stall = '1'     and flush = '0'                 else
	             2 when rising_edge(clk);
	
	
	IF_ir    <= dout_cache when ir_select = 0 else
		        IF_ir      when ir_select = 1 else
			    ZERO32     when ir_select = 2;
	

	-------------------- IF/ID Pipeline Register -----------------------------------
	ID <= ID_INIT          when rising_edge(clk) and flush = '1' else
	      ID               when rising_edge(clk) and stall = '1' else
	      (IF_ir, pc4reg)  when rising_edge(clk);

	BR <= BR_INIT when rising_edge(clk) and flush = '1' else
		  BR      when rising_edge(clk) and stall = '1' else  
	      BR_REG  when rising_edge(clk) ;
	
	entry_btb_reg <= entry_btb when rising_edge(clk);
	-------------------- Instruction Decode and register fetch (ID) ----------------

	dec : entity work.decoder
		port map(ID.ir, i);	
	
	ctrl : entity work.control
		port map(i, c);
		
	rf : entity work.regfile
		generic map(EDGE => FALLING)
		port map(clk, WB.c.regwr, i.Rs, i.Rt, WB.wa, WB_wd, rd1, rd2);

	wa <= i.Rd when c.regdst = '1' and c.link = '0' else -- R-Type
		  i.Rt when c.regdst = '0' and c.link = '0' else -- I-Type, lw
		  "11111";                                       -- JAL            

	-- sign extended immediate value
	signext <= X"ffff" & i.Imm when (i.Imm(15) = '1' and c.signext = '1') else
	           X"0000" & i.Imm;

	
	-- forward detection
	forwardA <= fromALUe when (i.Rs /= REG_ZERO) and (i.Rs = EX.wa) and (EX.c.regwr = '1') else
				fromALUm when (i.Rs /= REG_ZERO) and (i.Rs = MA.wa) and (MA.c.regwr = '1') and (MA.c.mem2reg = '0') else 
				fromMEM  when (i.Rs /= REG_ZERO) and (i.Rs = MA.wa) and (MA.c.regwr = '1') and (MA.c.mem2reg = '1') else
				fromReg;

	forwardB <= fromALUe when (i.Rt /= REG_ZERO) and (i.Rt = EX.wa) and (EX.c.regwr = '1') else 
				fromALUm when (i.Rt /= REG_ZERO) and (i.Rt = MA.wa) and (MA.c.regwr = '1') and (MA.c.mem2reg = '0') else
				fromMEM  when (i.Rt /= REG_ZERO) and (i.Rt = MA.wa) and (MA.c.regwr = '1') and (MA.c.mem2reg = '1') else
				fromReg;
					
	
	-- forwarding logic
	a <= rd1 	   when forwardA = fromReg  else
		 
		 aluout    when forwardA = fromALUe and EX.c.link = '0' else
		 EX.pc4    when forwardA = fromALUe and EX.c.link = '1' else
		 
		 MA.aluout when forwardA = fromALUm and MA.c.link = '0' else
		 MA.pc4    when forwardA = fromALUm and MA.c.link = '1' else
		 
		 WB_rd     when forwardA = fromMEM;
		 
	b <= rd2 	   when forwardB = fromReg  else
		
		 aluout    when forwardB = fromALUe and EX.c.link = '0' else
		 EX.pc4    when forwardB = fromALUe and EX.c.link = '1' else
		 
		 MA.aluout when forwardB = fromALUm and MA.c.link = '0' else
		 MA.pc4    when forwardB = fromALUm and MA.c.link = '1' else
		 
		 WB_rd     when forwardB = fromMEM;
		 
	-- detecting RAW hazard after load
	stall <= '1' when EX.c.mem2reg = '1' and (forwardA = fromALUe or forwardB = fromALUe) else
			 '0';	 
	
	-- comparator
  	invb    <= signed(not b);
  	twosC_b <= signed(invb) + signed(zeros31 & '1');
 	sum     <= to_slv(signed(a) + twosC_b); 
	
	zero   <= '1' when sum = X"00000000" else '0';
  	lez    <=     zero or      sum(31);
  	ltz    <= not zero and     sum(31);
  	gtz    <= not zero and not sum(31);
	
	
	-- Effective address calculation              
	pcbranch <= to_slv(signed(ID.pc4) + signed(signext(29 downto 0) & "00"));

	pcjump   <= ID.pc4(31 downto 28) & i.BrTarget & "00";
	
	
	is_brinst <= '1' when i.Mnem = BEQ  or i.Mnem = BGTZ or i.Mnem = BLEZ or
	                      i.Mnem = BLTZ or i.Mnem = BNE                   else
	             '0';
	
	-- BTB
	flush <= '1' when (BR.takeandhit = '0' and (c.jr = '1' or c.jump = '1' or branch = '1')) or
	                  (BR.takeandhit = '1' and not (c.jr = '1' or c.jump = '1' or branch = '1')) or
	                  (BR.takeandhit = '1' and c.jr = '1' and entry_btb_reg /= a) or
	                  (BR.takeandhit = '1' and c.jump = '1' and entry_btb_reg /= pcjump) or
	                  (BR.takeandhit = '1' and branch = '1' and entry_btb_reg /= pcbranch) else
	         '0';
	
	wr_btb <= '1' when (BR.takeandhit = '0' and (c.jr = '1' or c.jump = '1' or branch = '1')) or
	                   (BR.takeandhit = '1' and c.jr = '1' and entry_btb_reg /= a) or
	                   (BR.takeandhit = '1' and c.jump = '1' and entry_btb_reg /= pcjump) or
	                   (BR.takeandhit = '1' and branch = '1' and entry_btb_reg /= pcbranch) else
	           '0';	                  
	
	new_pc <= pcbranch when  branch = '1' else
	          pcjump   when  c.jump = '1' else
	          a        when  c.jr   = '1' else
	          ID.pc4;
	
	
	newentry_btb <= pcbranch when branch = '1' else
	                pcjump   when c.jump = '1' else
	                a        when c.jr   = '1';
	
	correct(to_i(BR.pc(6 downto 2))) <= correct(to_i(BR.pc(6 downto 2))) + 1 
	when is_brinst = '1' and BR.take_bht = branch and rising_edge(clk);
		
	
	incorrect(to_i(BR.pc(6 downto 2))) <= incorrect(to_i(BR.pc(6 downto 2))) + 1
	when is_brinst = '1' and BR.take_bht /= branch and rising_edge(clk);
	
	update_bht <= '1' when is_brinst = '1' else
	              '0';
	wastaken_bht <= branch;
	
	indexwd_bht <= BR.pc(6 downto 2);
	
	-- should it not be i.Mnem = BEQ  etc. ?
	branch <= '1' when (i.Opc = I_BEQ.Opc  and zero = '1')     or 
	                   (i.Opc = I_BNE.Opc  and not zero = '1') or 
	                   (i.Opc = I_BLEZ.Opc and lez = '1')      or 
	                   (i.Opc = I_BLTZ.Opc and ltz = '1')      or 
	                   (i.Opc = I_BGTZ.Opc and gtz = '1')      else 
	          '0';

	-------------------- ID/EX Pipeline Register -----------------------------------

	--TODO Explain why remove rd2
	EX <= EX_INIT when rising_edge(clk) and (stall = '1') else
	      (c, i, wa, a, b, signext, ID.pc4) when rising_edge(clk);

	-------------------- Execution Phase (EX) --------------------------------------

	rd2imm <= EX.imm when EX.c.alusrc = '1' else EX.b;

	alu_inst : entity work.alu(withBarrelShift)
		port map(EX.a, rd2imm, EX.c.aluctrl, EX.i.Shamt, aluout);

	-------------------- EX/MA Pipeline Register -----------------------------------

	MA <= (EX.c, EX.i, EX.wa, EX.a, EX.imm, EX.pc4, EX.b,
		  pcbranch, pcjump, aluout, zero, lez, ltz, gtz) when rising_edge(clk);

	-------------------- Memory Access Phase (MA) ----------------------------------

	wd     <= MA.b;    --b;                         
	aout   <= MA.aluout;

	dmem : entity work.bram_be			-- data memory  
		generic map(EDGE => Falling, FNAME => DFileName)
		port map(clk, MA.c, aout(12 downto 0), wd, WB_rd);

	-------------------- MA/WB Pipeline Register -----------------------------------

	WB <= (MA.c, MA.wa, MA.pc4, aout, WB_rd) when rising_edge(clk);

	-------------------- Write back Phase (WB) -------------------------------------

	WB_wd <= WB.wd   when WB.c.mem2reg = '1' and WB.c.link = '0' else -- from DMem
		     WB.aout when WB.c.mem2reg = '0' and WB.c.link = '0' else -- from ALU
		     WB.pc4;                                                  -- ret. Addr 

	writedata <= wd;
	dataadr   <= aout;
	memwrite  <= c.memwr;

end;
