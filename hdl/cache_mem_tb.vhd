library IEEE;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
use work.CASTS.all;
use work.mips_pkg.all;

entity cache_mem_tb is
end;

architecture test of cache_mem_tb is
	constant DATA_WIDTH   : integer  := 32;
	constant INDEX_WIDTH  : integer  := 8;
	constant TAG_WIDTH    : integer  := 4;
	constant OFFSET_WIDTH : integer  := 4;
	constant EDGE         : EdgeType := RISING;
	
	constant LINE_WIDTH   : integer  := (2**OFFSET_WIDTH)*8;
	constant HALF_WIDTH   : integer  := DATA_WIDTH/2;


	signal clk, 
	       we,
	       is_hit, 
	       is_valid, 
	       is_dirty    : std_logic;
	 
	signal addr        : std_logic_vector(TAG_WIDTH+INDEX_WIDTH+OFFSET_WIDTH-1 downto 0);
	signal din         : std_logic_vector(LINE_WIDTH-1 downto 0);
	signal access_type : AccessType;

	signal tag_prev    : std_logic_vector(TAG_WIDTH-1 downto 0);
	signal dout        : std_logic_vector(LINE_WIDTH-1 downto 0);
	

begin

	cache_mem_inst : entity work.cache_mem
		generic map(
			DATA_WIDTH   => DATA_WIDTH,
			INDEX_WIDTH  => INDEX_WIDTH,
			TAG_WIDTH    => TAG_WIDTH,
			OFFSET_WIDTH => OFFSET_WIDTH,
			EDGE         => EDGE
		)
		port map(
			clk         => clk,
			we          => we,
			addr        => addr,
			din         => din,
			access_type => access_type,
			is_hit      => is_hit,
			is_valid    => is_valid,
			is_dirty    => is_dirty,
			tag_prev    => tag_prev,
			dout        => dout
		);

	-- generate clock
	process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

	sim : process is
		variable word0, word1, word2, word3 : std_logic_vector(DATA_WIDTH - 1 downto 0);
		variable line0, line1, line2, line3 : std_logic_vector((2**OFFSET_WIDTH)*8 - 1 downto 0);
		variable half0, half1               : std_logic_vector((DATA_WIDTH / 2) - 1 downto 0);
		variable byte0, byte1               : std_logic_vector(7 downto 0);

	begin
		
		byte0 := x"aa";
		byte1 := x"bb";
	
		half0 := x"cc_cc";
		half1 := x"dd_dd";
	
		word0 := half1 & half0;
		word1 := half0 & byte1 & byte0;
		word2 := x"11_11_11_11";
		word3 := x"22_22_22_22";	
		
		line0 := word3 & word2 & word1 & word0; -- 0_00_0
		line1 := word2 & word1 & word0 & word3; -- 0_01_0
		line2 := word1 & word0 & word3 & word2; -- 0_02_0
		line3 := word0 & word3 & word2 & word1; -- 1_03_0
		wait for 1 ns;

		-- write some lines
		we          <= '1';
		addr    <= x"0_00_0";
		din         <= line0;
		access_type <= CacheLine;
		wait until falling_edge(clk);
		
		we          <= '1';
		addr    <= x"0_01_0";
		din         <= line1;
		access_type <= CacheLine;
		wait until falling_edge(clk);
		
		we          <= '1';
		addr    <= x"0_02_0";
		din         <= line2;
		access_type <= CacheLine;
		wait until falling_edge(clk);
		
		we          <= '1';
		addr    <= x"1_03_0";
		din         <= line3;
		access_type <= CacheLine;
		wait until falling_edge(clk);
		
		-- read line0
		we          <= '0';
		addr    <= x"0_00_0";
		access_type <= CacheLine;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = line0 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		-- read line3
		we          <= '0';
		addr    <= x"1_03_0";
		access_type <= CacheLine;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = line3 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		-- different tag
		we          <= '0';
		addr    <= x"2_03_0";
		access_type <= CacheLine;
		wait for 1 ns;
		assert is_hit = '0' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		
		-- read word2 in line3
		we          <= '0';
		addr    <= x"1_03_4";
		access_type <= Word;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto DATA_WIDTH => '0') & word2 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		
		-- read word0 in line3
		we          <= '0';
		addr    <= x"1_03_c";
		access_type <= Word;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto DATA_WIDTH => '0') & word0 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		
		-- read half0 in word0 in line3
		we          <= '0';
		addr    <= x"1_03_c";
		access_type <= Half;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto HALF_WIDTH => '0') & half0 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		-- read byte1 in word1 in line3
		we          <= '0';
		addr    <= x"1_03_1";
		access_type <= Byte;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto 8 => '0') & byte1 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		
		-- write word1 in word0 in line3
		we          <= '1';
		addr    <= x"1_03_c";
		din         <= (LINE_WIDTH-1 downto DATA_WIDTH => '0') & word1;
		access_type <= Word;
		wait until falling_edge(clk);
		
		-- read now word1 in line3
		we          <= '0';
		addr    <= x"1_03_c";
		access_type <= Word;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '1' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto DATA_WIDTH => '0') & word1 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		-- read half0 in word1 in line3
		we          <= '0';
		addr    <= x"1_03_e";
		access_type <= Half;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '1' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto HALF_WIDTH => '0') & half0 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		-- read byte1 in word1 in line3
		we          <= '0';
		addr    <= x"1_03_d";
		access_type <= Byte;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '1' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = (LINE_WIDTH-1 downto 8 => '0') & byte1 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
		
		
		-- write line3 again
		we          <= '1';
		addr    <= x"1_03_0";
		din         <= line3;
		access_type <= CacheLine;
		wait until falling_edge(clk);
		
		-- read line3
		we          <= '0';
		addr    <= x"1_03_0";
		access_type <= CacheLine;
		wait for 1 ns;
		assert is_hit = '1' report "is_hit unexpected value" severity FAILURE;
		assert is_dirty = '0' report "is_dirty unexpected value" severity FAILURE;
		wait until rising_edge(clk);
		wait for 1 ns;
		assert dout = line3 report "dout unexpected value" severity FAILURE;
		wait until falling_edge(clk);
				

		wait;

	end process sim;

end architecture test;

