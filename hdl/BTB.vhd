library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mips_pkg.all;
use work.casts.all;



entity BTB is
	port (
		clk, wr_btb, jpi   : in  std_logic;
		pc, btac_entry     : in  std_logic_vector(31 downto 0);
		target_addr        : out std_logic_vector(31 downto 0) := (others => '0');
		hit_btac, isit_jpi : out std_logic := '0' 
	);
end entity BTB;


architecture arch of BTB is
	
	signal we_cache,
	       hit_cache : std_logic_vector(1 downto 0) := (others => '0');
	
	signal dout_cache0, 
	       dout_cache1  : std_logic_vector(31 downto 0) := (others => '0');	
	
	signal index : std_logic_vector(3 downto 0) := (others => '0');
	
	signal hit,
	       selector,
	       rnd_bit   : std_logic := '0';
	           
	signal jpi0, jpi1 : std_logic_vector(15 downto 0) := (others => '0');
begin
	hit_btac <= hit;
	index    <= pc(5 downto 2);
	
	
	hit      <= '1' when hit_cache /= "00" and wr_btb = '0' else -- hit = '0' if currently writing
	            '0';

	selector <= '0' when rising_edge(clk) and hit_cache(0) = '1' else
	            '1' when rising_edge(clk) and hit_cache(1) = '1';
	
	
	target_addr <= dout_cache0 when selector = '0' else
	               dout_cache1 when selector = '1';
	
	we_cache    <= "01" when rnd_bit = '0' and wr_btb = '1' else
	               "10" when rnd_bit = '1' and wr_btb = '1' else
	               "00"; -- else read
	
	jpi0(to_i(index)) <= '1' when rising_edge(clk) and we_cache(0) = '1' and jpi = '1' else
	                     '0' when rising_edge(clk) and we_cache(0) = '1' and jpi = '0';
	                     
	jpi1(to_i(index)) <= '1' when rising_edge(clk) and we_cache(1) = '1' and jpi = '1' else
	                     '0' when rising_edge(clk) and we_cache(1) = '1' and jpi = '0';
	
	isit_jpi <= jpi0(to_i(index)) when hit_cache(0) = '1' else
	            jpi1(to_i(index)) when hit_cache(1) = '1';
	
	rnd_strategy : process(clk, hit)
	begin
		if hit = '0' and rising_edge(clk) then
			rnd_bit <= not rnd_bit; -- flip the bit
		end if;
	end process rnd_strategy;	


	block0 : entity work.cache_mem
		generic map(
			DATA_WIDTH   => 32,
			INDEX_WIDTH  => 4,
			TAG_WIDTH    => 26,
			OFFSET_WIDTH => 2,
			EDGE         => RISING
		)
		port map(
			clk         => clk,
			we          => we_cache(0),
			addr        => pc,
			din         => btac_entry,
			access_type => CacheLine,
			is_hit      => hit_cache(0),
			dout        => dout_cache0
		);
			
	block1 : entity work.cache_mem
		generic map(
			DATA_WIDTH   => 32,
			INDEX_WIDTH  => 4,
			TAG_WIDTH    => 26,
			OFFSET_WIDTH => 2,
			EDGE         => RISING
		)
		port map(
			clk         => clk,
			we          => we_cache(1),
			addr        => pc,
			din         => btac_entry,
			access_type => CacheLine,
			is_hit      => hit_cache(1),
			dout        => dout_cache1
		);
	
end architecture arch;

