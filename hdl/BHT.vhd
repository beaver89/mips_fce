library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mips_pkg.all;
use work.casts.all;

entity BHT is
	generic(
		EDGE       : EdgeType := RISING;
		INDEX_SIZE : positive := 5
	);

	port(
		clk, 
		update, 
		was_taken       : in  std_logic;
		
		index, 
		indexto_update  : in  std_logic_vector(INDEX_SIZE - 1 downto 0);
		
		take            : out std_logic := '0'
	);
end entity BHT;

architecture behav of BHT is
	constant STRONG_NOTTAKEN : std_logic_vector(1 downto 0) := "00";
	constant WEAK_NOTTAKEN   : std_logic_vector(1 downto 0) := "01";
	constant WEAK_TAKEN      : std_logic_vector(1 downto 0) := "10";
	constant STRONG_TAKEN    : std_logic_vector(1 downto 0) := "11";
	
	signal entry, 
	       entryto_update, 
	       prbits_update     : std_logic_vector(1 downto 0) := "00";
begin
	regfile_inst : entity work.regfile
		generic map(
			EDGE       => EDGE,
			DATA_WIDTH => 2,
			ADDR_WIDTH => INDEX_SIZE
		)
		port map(
			clk => clk,
			we3 => update,
			ra1 => index,
			ra2 => indexto_update,
			wa3 => indexto_update,
			wd3 => prbits_update,
			rd1 => entry,
			rd2 => entryto_update
		);

	take <= '0' when entry = STRONG_NOTTAKEN or entry = WEAK_NOTTAKEN else 
	        '1' when entry = WEAK_TAKEN or entry = STRONG_TAKEN;
	         
	
	
	-- update mechanism (used my MARS)
	hysteresis_counter : process (entryto_update, was_taken, update) is
	begin
		if update = '1' then
			if entryto_update = STRONG_NOTTAKEN then    -- Strongly Not Taken
				if was_taken = '0' then -- not taken
					prbits_update <= STRONG_NOTTAKEN; 
				else
					prbits_update <= WEAK_NOTTAKEN;
				end if;
			
			elsif entryto_update = WEAK_NOTTAKEN then -- Weakly Not Taken
				if was_taken = '0' then -- not taken
					prbits_update <= STRONG_NOTTAKEN;
				else
					prbits_update <= STRONG_TAKEN;
				end if;
			elsif entryto_update = WEAK_TAKEN then -- Weakly Taken
				if was_taken = '0' then -- not taken
					prbits_update <= STRONG_NOTTAKEN;
				else
					prbits_update <= STRONG_TAKEN;
				end if;
			else                             -- Strongly Taken	
				if was_taken = '0' then  -- not taken
					prbits_update <= WEAK_TAKEN;
				else
					prbits_update <= STRONG_TAKEN;
				end if;
			end if;
		end if;
		
	end process hysteresis_counter;
	
	
--	-- update mechanism
--	saturation_counter : process (entryto_update, was_taken, update) is
--	begin
--		if update = '1' then
--			if entryto_update = STRONG_NOTTAKEN then    -- Strongly Not Taken
--				if was_taken = '0' then -- not taken
--					prbits_update <= STRONG_NOTTAKEN; 
--				else
--					prbits_update <= WEAK_NOTTAKEN;
--				end if;
--			
--			elsif entryto_update = WEAK_NOTTAKEN then -- Weakly Not Taken
--				if was_taken = '0' then -- not taken
--					prbits_update <= STRONG_NOTTAKEN;
--				else
--					prbits_update <= WEAK_TAKEN;
--				end if;
--			elsif entryto_update = WEAK_TAKEN then -- Weakly Taken
--				if was_taken = '0' then -- not taken
--					prbits_update <= WEAK_NOTTAKEN;
--				else
--					prbits_update <= STRONG_TAKEN;
--				end if;
--			else                             -- Strongly Taken	
--				if was_taken = '0' then  -- not taken
--					prbits_update <= WEAK_TAKEN;
--				else
--					prbits_update <= STRONG_TAKEN;
--				end if;
--			end if;
--		end if;
--		
--	end process saturation_counter;
	
	

end architecture behav;
