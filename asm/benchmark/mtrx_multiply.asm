# Constants
.eqv FB_LENGTH 1024 # 16*16*4

.data
	C: .space FB_LENGTH
	A: .space FB_LENGTH
	B: .space FB_LENGTH
	
	LN_BREAK: .asciiz "\n"
	SPACE: .asciiz " "
	REG: .word 1

.text
	# $t0 = 16, $t1 = i, $t2 = j
	li $t0, 16
	li $t1, 0 # i = 0
	la $s0, A # load address of A array
	la $s1, B # laod address of B array
	la $s2, C # load address of C array
	
	
FOR_1:
	beq $t1, $t0, FOR_1_DONE # if i==16 goto FOR_1_DONE
	li $t2, 0 # j=0
FOR_2:
	beq $t2, $t0, FOR_2_DONE # if j==16 goto FOR_2_DONE
	
	sll $s3, $t1, 6 # i*64 
	sll $s4, $t2, 2 # j*4
	add $s3, $s3, $s4 # i*64 + j*4
	
	add $s4, $s0, $s3 # Addr(A)+ i*64 + j*4
	add $s5, $s1, $s3 # Addr(B)+i*64 + j*4
	
	subu $sp, $sp, 36 # Make a stack frame of 40 bytes
	sw $t0, 32($sp)
	sw $t1, 28($sp)
	sw $t2, 24($sp)
	sw $s0, 20($sp)
	sw $s1, 16($sp)
	sw $s2, 12($sp)
	sw $s3, 8($sp)
	sw $s4, 4($sp) # Address for A
	sw $s5, 0($sp) # Address for B

	jal lfsr
	srl $v0, $v0, 8 # lfsr() >> 8
	lw $s0, 4($sp) # load address for A
	sw $v0, 0($s0) # store lfsr() >> 8
	
	jal lfsr
	srl $v0, $v0, 8 # lfsr() >> 8
	lw $s0, 0($sp) # load address for B
	sw $v0, 0($s0) # store lfsr() >> 8
	
	# restore stack
	lw $t0, 32($sp)
	lw $t1, 28($sp)
	lw $t2, 24($sp)
	lw $s0, 20($sp)
	lw $s1, 16($sp)
	lw $s2, 12($sp)
	lw $s3, 8($sp)
	lw $s4, 4($sp)
	lw $s5, 0($sp) 

	addi $t2, $t2, 1 # j++
	j FOR_2
FOR_2_DONE:
	addi, $t1, $t1, 1 # i++
	j FOR_1

FOR_1_DONE:
	li, $t1, 0 # i=0

FOR_3:
	beq $t1, $t0, END # if i == 16 goto END
	li $t2, 0 # j=0
FOR_4:
	beq, $t2, $t0, FOR_4_DONE # if j==16 goto FOR_4_DONE
	sll $s3, $t1, 6 # i*64 
	sll $s4, $t2, 2 # j*4
	add $t4, $s3, $s4 # i*64 + j*4
	add $t4, $s2, $t4 # Addr(C)+i*64+j*4
	sw $zero, 0($t4) # c[i][j] = 0
	
	li $t3, 0 # $t3 = k = 0
FOR_5:
	beq $t3, $t0, FOR_5_DONE # if k==16 goto FOR_5_DONE
	# available registers: s6..., t5...
	
	sll $s6, $t3, 2 # k*4
	sll $s7, $t3, 6 # k*64
	
	add $t5, $s0, $s3 # Addr(A)+i*64
	add $t5, $t5, $s6 # Addr(A)+i*64+k*4
	
	add $t6, $s1, $s7 # Addr(B)+k*64
	add $t6, $t6, $s4 # Addr(B)+k*64+j*4
	
	# $t4 = Addr(c[i][j])
	# $t5 = Addr(a[i][k])
	# $t6 = Addr(b[k][j])
	
	# available registers: s6..s7, t7..
	
	lw $s6, 0($t4) # c[i][j]
	lw $t5, 0($t5) # a[i][k]
	lw $t6, 0($t6) # b[k][j]
	
	mul $t5, $t5, $t6
	add $t5, $t5, $s6
	sw $t5, 0($t4)
	
	add $a0, $t5, $zero 
	add $t9, $t5, $zero
	li $v0, 1 # load instruction id for print integer
	syscall # print integer
	
	li $v0, 4
	la $a0, SPACE
	syscall # printf("\n");
	
	addi $t3, $t3, 1 # k++
	j FOR_5

FOR_5_DONE:
	li $v0, 4
	la $a0, LN_BREAK
	syscall # printf("\n");
	
	addi $t2, $t2, 1 # j++
	j FOR_4
	
FOR_4_DONE:
	addi $t1, $t1, 1 # i++
	j FOR_3

END:
	li $v0, 10
	syscall


lfsr:	# $t0=i, $t1 = 16, $t3 = reg
	li $t0, 0 # i = 0
	li $t1, 16 # $t1 = 16
	la $t2, REG # load adress of the REG data
	lhu $t3, 0($t2) # $t3 = REG load halfword unsigned

loop2:	beq $t0, $t1, done2 # if i==16 goto done2
	addi $t0, $t0, 1 # i++	
	srl $s0, $t3, 0 # reg >> 0
	srl $s1, $t3, 2 # reg >> 2
	srl $s2, $t3, 3 # reg >> 3
	srl $s3, $t3, 5 # reg >> 5
	srl $s4, $t3, 1 # reg >> 1
	xor $t4, $s0, $s1 # (reg >> 0) ^ (reg >> 2)
	xor $t4, $t4, $s2 # (reg >> 0) ^ (reg >> 2) ^ (reg >> 3)
	xor $t4, $t4, $s3 # (reg >> 0) ^ (reg >> 2) ^ (reg >> 3) ^ (reg >> 5)
	sll $t4, $t4, 15 # highest << 15
	or $t3, $s4, $t4 # (reg >> 1) | (highest << 15)
	andi $t3, $t3, 0xffff
	sw $t3, 0($t2) # store only the lower 16 bits in reg
	j loop2
done2:
	add $v0, $zero, $t3
	jr $ra

	
	
	
	
	
	
