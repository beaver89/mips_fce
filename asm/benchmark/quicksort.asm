        
.data
.align  2

values:		.word  9, 4, 1, 2, 16, 3, 5, 8, 9, 19, 1, 7, 8, 13, 18, 1	   
valCount:  	.word  16
#-------------------------------------------------------------------------------------------------------------------------------

.text
.globl  main
      

main:     
        lw      $s1, valCount  # s2: number of values to sum (n)
        la  	$s2, values     # get array address


 # call sort routine       
       	la	$a0, values
       	li	$a1, 0
       	lw	$a2, valCount
       	# length - 1
       	addi	$a2, $a2, -1
        jal 	quickSort
        j endend
                                                                                                                                                                                     
# void quickSort
# a0 address of array
# a1 first element	(i)
# a2 last element    (j)  

# t0 = i
# t1 = j
# t3 = pivot

# s0 = left
# s1 = right	      		      	
quickSort:
	addi    $sp, $sp, -12        # allocate stack space for 3 values
        sw      $ra, 0($sp)         # store off the return addr, etc 
        sw      $s0, 4($sp)
        sw	$s1, 8($sp)
#------------------------------------------------------
	# i = left
	# j = right
	move 	$t0, $a1
	move 	$t1, $a2
	# save left and right values
	move 	$s0, $a1
	move 	$s1, $a2
	# index offset for middle
	add	$t3, $t0, $t1	
	srl	$t3, $t3, 1 # div $t3, $t3, 2
	mul	$t3, $t3, 4
	# array[left+right/2] -- pivot
	add	$t3, $t3, $a0	# add pivot index (with offset) to array's base address
	lw	$t3, 0($t3) # pivot value at array[left+right/2]
partition:	
	# while ( i <= j )
	bgt	$t0, $t1, endPartition
	
	
whileI:
	# get array[i]
	mul	$t5, $t0, 4
	add	$t5, $t5, $a0		
		
	lw	$t5, 0($t5)
	# while ( array[i] <= pivot )
	bge	$t5, $t3, whileJ
	addi	$t0, $t0, 1
	j	whileI

whileJ:	
	# get array[j]
	mul 	$t2, $t1, 4
	add	$t2, $a0, $t2
	lw	$t2, 0($t2)
	# while ( array[j] <= pivot )
	ble	$t2, $t3, contB
	addi	$t1, $t1, -1
	j	whileJ
contB:
	# if (i <= j)
	bgt	$t0, $t1, partition
	# swap section
	mul 	$t2, $t0, 4	# array [i]
	mul 	$t6, $t1, 4	# array [j]
	
	add	$t2, $t2, $a0
	add	$t6, $t6, $a0
	
	lw	$t7, 0($t2)	# array[i]
	lw	$t4, 0($t6)	# array[j]
	
	sw	$t7, 0($t6)	# array[j] = array[i]
	sw	$t4, 0($t2)	# array[i] = array[j]
	
	addi	$t0, $t0, 1
	addi	$t1, $t1, -1
		
	j 	partition	
endPartition:	
	# recursively sort the rest of the list

	bge	$s0, $t1, ifRight	# if left  < j
	la	$a0, values
	move	$a1, $s0
	move	$a2, $t1
	# quicksort(array, left, j)
	jal	quickSort	

ifRight:	
	bge	$t0, $s1, endQS	# if i < right
	la	$a0, values
	move	$a1, $t0
	move	$a2, $s1
	# quicksort(array, i, right)
	jal	quickSort
endQS:		 
#-------- END ------------	
        lw      $ra, 0($sp)         # load the return addr, etc 
        lw      $s0, 4($sp)
        lw	$s1, 8($sp)
	addi    $sp, $sp, 12 
	jr 	$ra
	

endend:
	nop
	nop
	nop
