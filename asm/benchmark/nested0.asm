# test for nested loops

	.data

tmp: .word 0xFFFF # just to generate a .dmem file

	.text

addi $3, $0, 5 # l = 32
addi $5, $0, 10 # m = 64
addi $7, $0, 4 # n = 128

addi $2, $0, 0  # i = 0
for0:
beq $2, $3, done0
	addi $4, $0, 0  # j = 0
	for1:
	beq $4, $5, done1
		addi $6, $0, 0  # k = 0
		for2:
		beq $6, $7, done2

		add $8, $8, 1 # increment counter

		addi $6, $6, 1
		j for2
		done2: 
	addi $4, $4, 1
	j for1
	done1:
addi $2, $2, 1	
j for0
done0:

nop
nop
nop


