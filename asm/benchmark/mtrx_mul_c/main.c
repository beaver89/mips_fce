#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int a[16][16], b[16][16], c[16][16];
static uint16_t reg = 0x1;


uint16_t lfsr () {
	int i;
	for (i = 0; i < 16; i++) {
		uint16_t highest = ((reg >> 0) ^ (reg >> 2) ^ (reg >> 3) ^ (reg >> 5));
		reg = (reg >> 1) | (highest << 15);
	}

	return reg;
}

int main(int argc, char *argv[]) {
	// generate random numbers for the matrices
	int i;
	int j;
	int k;
	for (i = 0; i < 16; i++)
		for (j = 0; j < 16; j++) {
			a[i][j] = lfsr() >> 8;
			b[i][j] = lfsr() >> 8;
			//c[i][j] = 0;
		}

	// compute matrix multiply
	for (i=0; i<16; i++)
		for(j =0; j<16; j++) {
			c[i][j] = 0;

			for(k = 0; k <16; k++ ) {
				c[i][j] = c[i][j] + a[i][k] * b[k][j];
				printf("%d ", c[i][j]);
			}

			printf("\n");
		}
	return(0);
}
