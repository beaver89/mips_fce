#include <math.h>
#include <stdio.h>


line(int x0, int y0, int x1, int y1, int r, int g, int b) {
	int dx = abs(x1 - x0);
	int dy = abs(y1 -y0);
	int sx = (x0 < x1) ? 1 : -1;
	int sy = (y0 < y1) ? 1 : -1;
	int err = (dx > dy) ? dx : -dy/2;
	int e2;

	for (;;) {
		printf("(%d,%d) \n", x0, y0); // set pixel (x,y)
		if (x0 == x1 && y0 == y1) break;
		e2=err;
		if (e2 > -dx) {err -= dy; x0 += sx;}
		if (e2 < dy) {err += dx; y0 += sy;}
	}
}

int main(){
	int x0, y0, x1, y1;
	
	x0 = 75;
	y0 = 200;
	x1 = 175;
	y1 = 200;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 175;
	y0 = 200;
	x1 = 175;
	y1 = 100;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 175;
	y0 = 100;
	x1 = 75;
	y1 = 100;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 75;
	y0 = 100;
	x1 = 75;
	y1 = 200;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 75;
	y0 = 200;
	x1 = 175;
	y1 = 100;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 75;
	y0 = 100;
	x1 = 175;
	y1 = 200;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 75;
	y0 = 100;
	x1 = 125;
	y1 = 50;
	line(x0, y0, x1, y1, 255, 255, 255);
	
	x0 = 125;
	y0 = 50;
	x1 = 175;
	y1 = 100;
	line(x0, y0, x1, y1, 255, 255, 255);

	return 0;
}




