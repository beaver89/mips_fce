.data
RESULT:
	.word 0

.text
	# $a0 = a, $a1 = b input parameters
	li $a0, 3528 # a = 3528
	li $a1, 3780 # b = 3780
# if (a==0)	
	bne $a0, $zero, ELSE1 # if a != 0, goto ELSE1
	add $v0, $a1, $zero # set result $v0 = b
# else
ELSE1:

# while(b != 0)
WHILE:
	beq $a1, $zero, ENDWHILE # if b == 0 goto ENDWILE
# if (a > b)
	ble $a0, $a1, ELSE2 # if a <= b goto ELSE2
	sub $a0, $a0, $a1 # a = a - b
	j WHILE # goto WHILE
# else		
ELSE2:
	sub $a1, $a1, $a0 # b = b - a
# end if
	j WHILE # goto WHILE

ENDWHILE:
# end while
	add $v0, $a0, $zero # set result $v0 = a
#endif

END:
	# store to memory
	la $t0, RESULT
	sw $v0, 0($t0) # store result in memory
	nop
	nop
