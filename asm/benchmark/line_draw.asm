# Constants
.eqv FB_LENGTH 262144 # 256*256*4
.eqv LINE_WIDTH 1024

.data
	FB: .space FB_LENGTH
.text

MAIN:
	li $s0, 0xFFFFFF # set white pixel color
	
	li $a0, 75
	li $a1, 200
	li $a2, 175
	li $a3, 200
	jal DRAW_LINE
	
	li $a0, 175
	li $a1, 200
	li $a2, 175
	li $a3, 100
	jal DRAW_LINE
	
	li $a0, 175
	li $a1, 100
	li $a2, 75
	li $a3, 100
	jal DRAW_LINE
	
	li $a0, 75
	li $a1, 100
	li $a2, 75
	li $a3, 200
	jal DRAW_LINE
	
	li $a0, 75
	li $a1, 200
	li $a2, 175
	li $a3, 100
	jal DRAW_LINE
	
	li $a0, 75
	li $a1, 100
	li $a2, 175
	li $a3, 200
	jal DRAW_LINE
	
	li $a0, 75
	li $a1, 100
	li $a2, 125
	li $a3, 50
	jal DRAW_LINE
	
	li $a0, 125
	li $a1, 50
	li $a2, 175
	li $a3, 100
	jal DRAW_LINE

	j END

# $a0 = x0,	$a1 = y0,	$a2 = x1, 	$a3 = y1
# $t0 = dx,	$t1 = dy, 	$t2 = sx, 	$t3 = sy,
# $t4 = err, 	$t5 = e2
# $s0 = pixel colour

DRAW_LINE:
	
	
# if (x0 < x1)
	slt $s1, $a0, $a2 # 1 if x0 < x1
	beqz $s1, ELSE1 
	sub $t0, $a2, $a0 # dx = x1 - x0
	li $t2, 1
	j IF1_END
# else	
ELSE1: 
	sub $t0, $a0, $a2 # dx = x0 - x1
	li $t2, -1
IF1_END:
# end if

# if (y0 < y1)
	slt $s1, $a1, $a3 # 1 if y0 < y1
	beqz $s1, ELSE2
	sub $t1, $a3, $a1 # dy = y1 - y0
	li $t3, 1
	j IF2_END
# else
ELSE2:
	sub $t1, $a1, $a3 # dy = y0 - y1
	li, $t3, -1
IF2_END:
# end if

# if (dx > dy)
	slt $s1, $t1, $t0 # 1 if dy < dx
	beqz $s1, ELSE3
	addi $t4, $t0, 0
	j IF3_END
# else
ELSE3:
	srl $t4, $t1, 1
	li $s1, -1
	mul $t4, $t4, $s1 # $s2 = -dy
	
IF3_END:
# end if

	li $s1, 1 # $s1 reserved for loop break
	li $s2, LINE_WIDTH
	la $s3, FB 
	li $s4, 4 # pixel size in bytes

# for(;;) until break
FOR1:
	beqz $s1, FOR1_END
	add $24, $0, $a0 # $24 output x coordinate
	add $25, $0, $a1 # $25 output y coordinate
	
	mul $s5, $a0, $s4 # x0 = x0 * 4
	mul $s6, $a1, $s2 # y0 = y0 * 1024
	add $s5, $s5, $s6 # (x0 * 4) + (y0 * 1024)
	add $s5, $s3, $s5 # Addr(FB) + (x0 * 4) + (y0 * 1024)
	sw $s0, 0($s5) # print x0, y0
	
# if (x0 == x1 && y0 == y1)
	bne $a0, $a2, IF1_FOR1_END
	bne $a1, $a3, IF1_FOR1_END
	li $s1, 0
	j FOR1	
IF1_FOR1_END:
# end if

	addi $t5, $t4, 0 # e2 = err
		
# if (e2 > -dx)
	li $s5, -1
	mul $s5, $s5, $t0 # dx * -1
	slt $s5, $s5, $t5 # 1 if -dx < e2
	beqz $s5, IF2_FOR1_END
	sub $t4, $t4, $t1 # error = error - dy
	add $a0, $a0, $t2 # x0 = x0 + sx
IF2_FOR1_END:	
# end if

# if (e2 < dy)
	slt $s5, $t5, $t1
	beqz $s5, IF3_FOR1_END
	add $t4, $t4, $t0 # err = err + dx
	add $a1, $a1, $t3 # y0 = y0 + sy	
IF3_FOR1_END:
# end if	
	j FOR1

FOR1_END:
 j DRAW_LINE_DONE

		
DRAW_LINE_DONE:
	jr $ra
	


END:
	nop
	nop
	nop
