.data
REG: .word 1
LN_BREAK: .asciiz "\n"

.text
.globl lfsr
main:	
	# $t0 = i, $t1 = 20
	li $t0, 0 # i=0
	li $t1, 20 #

loop1:
	beq $t0, $t1, done1 # if i==20 goto done1
	addi $t0, $t0, 1 # i++
	
	subu $sp, $sp, 8 # Make a stack frame of 8 bytes
	sw $t0, 4($sp) # save i
	sw $t1, 0($sp) # save $t1=20
	jal lfsr
	
	lw $t0, 4($sp) # load i from stack
	lw $t1, 0($sp) # load 20 from stack
	
	add $a0, $v0, $zero # store reasult in $a0
	
	j loop1 # return to loop
done1:
	j end
	

lfsr:	# $t0=i, $t1 = 16, $t3 = reg
	li $t0, 0 # i = 0
	li $t1, 16 # $t1 = 16
	la $t2, REG # load adress of the REG data
	lhu $t3, 0($t2) # $t3 = REG load halfword unsigned

loop2:	beq $t0, $t1, done2 # if i==16 goto done2
	addi $t0, $t0, 1 # i++	
	srl $s0, $t3, 0 # reg >> 0
	srl $s1, $t3, 2 # reg >> 2
	srl $s2, $t3, 3 # reg >> 3
	srl $s3, $t3, 5 # reg >> 5
	srl $s4, $t3, 1 # reg >> 1
	xor $t4, $s0, $s1 # (reg >> 0) ^ (reg >> 2)
	xor $t4, $t4, $s2 # (reg >> 0) ^ (reg >> 2) ^ (reg >> 3)
	xor $t4, $t4, $s3 # (reg >> 0) ^ (reg >> 2) ^ (reg >> 3) ^ (reg >> 5)
	sll $t4, $t4, 15 # highest << 15
	or $t3, $s4, $t4 # (reg >> 1) | (highest << 15)
	andi $t3, $t3, 0xffff
	sw $t3, 0($t2) # store only the lower 16 bits in reg
	j loop2
done2:
	add $v0, $zero, $t3
	jr $ra


end:
	nop
	nop
	
	
	
	
	 

