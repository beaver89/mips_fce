.data 
	val: .word 20, 30, 10, 40, 50, 60, 30, 25, 10, 5
	
.text

	# test beq
	addi $2, $0, 10		# $2 = 10
	addi $3, $0, 11		# $3 = 11
	beq  $2, $3, label1     # if $2 == $3 goto label1
	addi $4, $0, 1   
	addi $5, $0, 1
	addi $6, $0, 1
	
label1:
	# test bne
	addi $2, $0, 10		# $2 = 10
	addi $3, $0, 10		# $3 = 10
	bne  $2, $3, label2     # if $2 != $3 goto label2
	addi $4, $0, 2     
	addi $5, $0, 2
	addi $6, $0, 2
	
label2:
	# test blez
	addi $2, $0, 1		# $2 = 1
	blez $2, label3         # if $2 <= 0 goto label3
	addi $4, $0, 3    
	addi $5, $0, 3
	addi $6, $0, 3

label3:
	# test bltz
	addi $2, $0, 1		# $2 = 1
	bltz $2, label4         # if $2 < 0 goto label4
	addi $4, $0, 4     
	addi $5, $0, 4
	addi $6, $0, 4
	
label4:
	# test bgtz
	addi $2, $0, -1		# $2 = -1
	bgtz $2, label5         # if $2 > 0 goto label5
	addi $4, $0, 5    
	addi $5, $0, 5
	addi $6, $0, 5
	
label5:
	nop
	addi $4, $0, 4     
	addi $5, $0, 5
	addi $6, $0, 6
		
	
	
	
	
	