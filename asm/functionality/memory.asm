# load and store

.data

val:  .word  0x2222, 0x33994499, 0x5555, 0xffff1111, 0xffff7777


.text
      la $2, val	# address of val
      
      lw $3, 0($2)	# $3 = 0x2222
      lw $4, 8($2)	# $4 = 0x5555
      lw $5, 12($2)	# $5 = 0xffff1111
      
      lui $6, 0x1111	# $6 = 0x11110000
      lui $7, 0xffff	# $7 = 0xffff0000
      
      lb $8, 4($2)	# $8 = 0xffffff99  (sign extension)
      lb $9, 5($2)	# $9 = 0x44
      
      lbu $10, 4($2)	# $10 = 0x99
      lbu $11, 5($2)	# $11 = 0x44
      
      lh $12, 12($2)	# $12 = 0x1111
      lh $13, 14($2)	# $13 = 0xffffffff (sign extension)
      
      lhu $14, 12($2)	# $12 = 0x1111
      lhu $15, 14($2)	# $13 = 0x0000ffff (no sign extension)
      
      sw $8, 20($2)	# Mem[$2 + 20] <- 0xffffff99
      
      sb $8, 24($2)	# Mem[$2 + 24] <- 0x99
      
      li $16, 0xffff	# $16 = 0xffff
      li $17, 0x1111	# $17 = 0x1111
