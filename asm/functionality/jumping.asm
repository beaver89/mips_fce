.data 
	val: .word 20, 30, 10, 40, 50, 60, 30, 25, 10, 5
	
.text

	# test jump
	j label6		# goto label6
	addi $4, $0, 1     
	addi $5, $0, 2
	addi $6, $0, 3
	
label6:
	# test jal
	jal label7		# goto label7 and save the return address
	addi $4, $0, 4     
	addi $5, $0, 5
	addi $6, $0, 6
	
label7: 
	la $7, label8
	nop
	nop
	nop
	# test jr
	jr $7			# goto label8
	addi $4, $0, 7     
	addi $5, $0, 8
	addi $6, $0, 9
	
label8:
	nop
	addi $4, $0, 10     
	addi $5, $0, 11
	addi $6, $0, 12