
.data

val:	.word 20, 30, 10, 2



.text
main:
	addi $2, $0, 2
	addi $3, $0, 3
	addi $4, $0, 4
	addi $5, $0, 5
	la   $10, val
	
	nop
	nop
	nop
	
	#------------------------------------------------
	# forwardA = fromReg
	# forwardB = fromReg
	add $6, $2, $3		# $6 = 5 = 2 + 3
	
	
	# forwardA = fromALUe
	# forwardB = fromReg
	sub $7, $6, $2		# $7 = 3 = 5 - 2
	
	
	# forwardA = fromALUm
	# forwardB = fromReg
	or $8, $6, $3		# $8 = 7 = 5 or 3
	
	
	# forwardA = fromMEM
	# forwardB = fromReg
	lw  $9, 0($10)
	nop
	add $11, $9, $2		# $11 = 22 = 20 + 2
	
	
	#------------------------------------------------
	# forwardA = fromReg
	# forwardB = fromALUe
	add $6, $2, $11		# $6 = 24 = 2 + 22
	
	
	# forwardA = fromALUm
	# forwardB = fromALUe
	and $7, $11, $6		# $7 = 16 = 22 and 24
	
	# forwardA = fromMEM
	# forwardB = fromALUe
	lw  $9, 4($10)
	sub $2, $2, $0
	add $11, $9, $2		# $11 = 32 = 30 + 2
	
	
	#------------------------------------------------
	# forwardA = fromReg
	# forwardB = fromALUm
	or $6, $3, $2		# $6 = 3 = 3 or 2
	
	# forwardA = fromALUe
	# forwardB = fromALUm
	add $7, $6, $11		# $7 = 35 = 3 + 32
	
	
	#------------------------------------------------
	# forwardA = fromReg
	# forwardB = fromMEM
	lw  $9, 8($10)
	nop
	add $6, $2, $9		# $6 = 2 + 10
	
	
	# forwardA = fromALUe
	# forwardB = fromMEM
	lw  $9, 12($10)		# $9 = 2
	add $11, $0, $5		# $11 = 5
	sub $12, $11, $9	# $12 = 3 = 5 - 2
	
	
	
	
	